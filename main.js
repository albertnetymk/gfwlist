var http = require('http');
var exec = require('child_process').exec;

var content = null;
var pac = 'output.pac';
var proxy = 'SOCKS5 127.0.0.1:10086';
var gfw = "gfwlist2pac -f " + pac + " -p '" + proxy + "'";
var cmd = gfw + '>/dev/null; ' + 'cat ' + pac;

var refresh = function() {
  exec(cmd, function(err, stdout, stderr) {
    content = "// " + new Date() + "\n" + stdout;
  });
};

var recur = function() {
  refresh();
  setTimeout(recur, 1000*60*60*24);
}

recur();

http.createServer(function(req, res) {
  // TODO parseInt to get the port number
  // console.log(req.url); // '/<port>'
  res.setHeader('Content-Type', 'text/plain');
  res.setHeader('Cache-Content', 'public, max-age=86400');
  res.writeHeader(200);
  res.end(content);
}).listen(process.env.PORT || 8000);
